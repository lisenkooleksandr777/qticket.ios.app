//
//  BookedTicketDelegate.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 21.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

protocol BookedTicketOperationDelegate {
    
    func bookedTicketOperationDidFail(error: String)
    
    func bookedTicketOperationDidSuccess(ticket: BookedTicket)
}
