//
//  BookedTicketListOperationDelegate.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 14.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

protocol BookedTicketsOperationDelegate {
    
    func bookedTicketsOperationDidFail(error: String)
    
    func bookedTicketsOperationDidSuccess(tickets: Array<TicketPreview>)
}
