//
//  TicketBookingDelegate.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 13.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

protocol BookingTicketDelegate {
    
    func bookingTicketOperationDidFail(error: String)
        
    func bookingTicketOperationDidSuccess(ticket: Ticket)
}
