//
//  Validation+Extensions.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import ValidatedPropertyKit

extension Validation where Value == String {
    
    static func required(errorMessage: String = "Is Empty") -> Validation {
           return .init { value in
               value.isEmpty ? .failure(.init(message: errorMessage)) : .success(())
           }
    }
    
    static func maxLenth(errorMessage: String = "To long", length: Int = 0) -> Validation {
           return .init { value in
            (!value.isEmpty && value.count > length) ? .failure(.init(message: errorMessage)) : .success(())
        }
    }
}
