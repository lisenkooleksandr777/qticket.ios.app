//
//  ErrorAlertFactory.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import UIKit

public class ErrorAlertFactory {
    
    public static func GetErrorAlert(message: String) -> UIAlertController {
        
        let alertView = UIAlertController(title: " ", message: message, preferredStyle: UIAlertController.Style.alert)
        let image = UIImage(named: "icon-error")
        
        let imageView = UIImageView(frame: CGRect(x: 120, y: 10, width: image?.size.width ?? 0, height: image?.size.height ?? 0))
        
        imageView.image = image
        alertView.view.addSubview(imageView)
        
        return alertView
    }
}
