//
//  LocalizationUtil.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation

public class LocalizationUtil {
    
    public static func GetLocalizationString(id: String) -> String {
        return NSLocalizedString(id, tableName: "Localization", comment: "")
    }
    
    public static func GetLocalizationString(id: String, comment: String) -> String {
        return NSLocalizedString(id, tableName: "Localization", comment: comment)
    }
    
}
