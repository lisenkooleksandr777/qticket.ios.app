//
//  DateTime.swift
//  e.com
//
//  Created by Oleksandr Lisenko on 04.07.19.
//  Copyright © 2019 Oleksandr Lysenko. All rights reserved.
//

import Foundation


public class DateTimeUtil {
    
    private static let formatter = DateFormatter()
    private static let ISOformatter = ISO8601DateFormatter()
    
    static func formatDate(date: String) -> String{
        
        //2019-05-06T13:50:17.563+03:00
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        //formatter.timeZone = TimeZone(secondsFromGMT: 0)
        // convert your string to date
        let eventDateTime = formatter.date(from: date)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let eventDate = formatter.string(from: eventDateTime!)
        
        return eventDate
    }
    
    static func fromFBDate(interval: TimeInterval) -> String? {
        let date = Date(timeIntervalSince1970: interval)
        return formatDateISO8601(date: date)
    }
    
    static func formatDateISO8601 (date: Date) -> String {
        return ISOformatter.string(from: date)
    }
    
    static func formatDateISO8601 (string: String) -> Date? {
        return ISOformatter.date(from: string)
    }
    
    static func GetISO8601DateRange(from: String, to: String) -> String {
        
        formatter.dateFormat = "dd"
        var ds = ISOformatter.date(from: from)
        var de = ISOformatter.date(from: to)
        
        let ds_day = formatter.string(from: ds!)
        let de_day = formatter.string(from: de!)
        
        formatter.dateFormat = "MMMM"
        
        ds = ISOformatter.date(from: from)
        de = ISOformatter.date(from: to)
        
        let ds_m = formatter.string(from: ds!)
        let de_m = formatter.string(from: de!)
        
        formatter.dateFormat = "yyyy"
        
        ds = ISOformatter.date(from: from)
        de = ISOformatter.date(from: to)
        
        let ds_y = formatter.string(from: ds!)
        let de_y = formatter.string(from: de!)
        
        if(ds_y == de_y){
            if(ds_m == de_m){
                if(ds_day == de_day){
                    return "\(ds_day) \(ds_m) \(ds_y)"
                }
                else{
                    return  "\(ds_day)-\(de_day) \(ds_m) \(ds_y)"
                }
            }
            else{
                return "\(ds_day) \(ds_m) - \(de_day) \(de_m) \(de_y)"
            }
        }
        else{
            return "\(ds_day) \(ds_m) \(ds_y) - \(de_day) \(de_m) \(de_y)"
        }
    }
}

