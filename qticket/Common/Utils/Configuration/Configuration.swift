//
//  Configuration.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class Configuration {
    
    static var AuthServiceEndPoint: String!
    static var TicketServiceEndPoint: String!
    
    private static let bookedticketQRPath = "/api/v1/Ticket/qr"
    
    static func Configure() {
        Configuration.ConfigureServices()
    }
    
    private static func ConfigureServices() {
        if let services: [String: String] = getPlist(withName: "Services") {
            Configuration.AuthServiceEndPoint = services["AUTH_SERVICE_END_POINT"]
            Configuration.TicketServiceEndPoint = services["TICKET_SERVICE_END_POINT"]
        }
    }
    
    static func getBookedTicketQRURL(id: Int) -> String {
         return "\(Configuration.TicketServiceEndPoint!)\(self.bookedticketQRPath)/\(id)"
    }
    
    private static func getPlist(withName name: String) -> [String:String]?
    {
        if  let path = Bundle.main.path(forResource: name, ofType: "plist"),
            let xml = FileManager.default.contents(atPath: path),
            let cofig = try? PropertyListDecoder().decode([String:String].self, from: xml)
            {
                return cofig
            }
        return nil
    }
}
