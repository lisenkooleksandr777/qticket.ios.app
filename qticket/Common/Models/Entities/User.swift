//
//  User.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 10.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class User: Codable {
    var id: Int!
    var name: String!
    var pictureURL: String?
}
