//
//  TicketPreview.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 14.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class TicketPreview: Codable {
    var id: Int!
    var name:  String!
    var dateStart: String!
    var dateEnd: String!
}
