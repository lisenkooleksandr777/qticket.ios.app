//
//  EventTicket.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class EventTicket: Codable {
    var id: Int! = 0
    var name: String!
    var description: String!
    var amount: Int!
    var price: Double! = 0.0
    var dateStart: String!
    var dateEnd: String!
}
 
