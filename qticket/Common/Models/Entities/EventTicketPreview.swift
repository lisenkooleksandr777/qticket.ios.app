//
//  EventTicketPreview.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

class EventTicketPreview: Codable {
    var id: Int!
    var name:  String!
    var dateStart: String!
    var dateEnd: String!
}
