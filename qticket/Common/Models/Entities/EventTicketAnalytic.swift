//
//  EventTicketAnalytic.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 10.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation


class EventTicketAnalytic: Codable {
    var eventTicket: EventTicket!
    var amountOfAvailableTickets: Int!
}
