//
//  Ticket.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 10.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class Ticket: Codable {
    var id: Int
    var event: EventTicket
    var owner: User
    var visitorName: String
}


class BookedTicket: Codable {
    var id: Int
    var event: EventTicket
    var visitorName: String
}
