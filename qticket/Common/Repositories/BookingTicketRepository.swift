//
//  TicketRepository.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 13.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class BookingTicketRepository: BaseRepository {
    
    private let bookticketPath = "/api/v1/Ticket/book"
    private let bookedticketsPath = "/api/v1/Ticket/booked"
    private let bookedticketPath = "/api/v1/Ticket"
    
    
    func bookedTicket(id: Int, callBack: @escaping (BookedTicket?, Error?) -> Void) {
        let baseURL = "\(Configuration.TicketServiceEndPoint!)\(self.bookedticketPath)/\(id)"
       
        let url = URL(string: baseURL)!
        
        let headers = HttpHeaderBuilder().addJsonContentType().addAuthenticationToken().Result()
        super.get(url: url, headers: headers, completion: callBack)
    }
    
    func bookedTickets(page: Int, from: String?, to: String?, callBack: @escaping (Array<TicketPreview>?, Error?) -> Void) {
        var baseURL = "\(Configuration.TicketServiceEndPoint!)\(self.bookedticketsPath)?page=\(page)"
       
        if let f = from {
            baseURL = "\(baseURL)&dateFrom=\(f)"
        }
        
        if let t = to {
            baseURL = "\(baseURL)&dateTo=\(t)"
        }
        
        let url = URL(string: baseURL)!
        
        let headers = HttpHeaderBuilder().addJsonContentType().addAuthenticationToken().Result()
        super.get(url: url, headers: headers, completion: callBack)
    }
    
    func bookTicket(ticketToBook: BookTicketEntity, callBack: @escaping (Ticket?, Error?) -> Void) {
        let url = URL(string: "\(Configuration.TicketServiceEndPoint!)\(self.bookticketPath)")!
        
        let headers = HttpHeaderBuilder().addJsonContentType().addAuthenticationToken().Result()
        super.post(data:ticketToBook,  url: url, headers: headers, completion: callBack)
    }
}
