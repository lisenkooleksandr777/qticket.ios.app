//
//  TicketRepository.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class TicketRepository: BaseRepository {
    private let ticketEndPoint = "/api/v1/TicketAgregator"
    
    func createTicket(eventTicket: EventTicket, callBack: @escaping (EventTicket?, Error?) -> Void) {
        let url = URL(string: "\(Configuration.TicketServiceEndPoint!)\(self.ticketEndPoint)")!
        
        let headers = HttpHeaderBuilder().addJsonContentType().addAuthenticationToken().Result()
        super.post(data: eventTicket, url: url, headers: headers, completion: callBack)
    }
    
    func getOwnTickets(page: Int!, itemsPerPage: Int!, callBack: @escaping (Array<EventTicketPreview>?, Error?) -> Void) {
        let urlString = "\(Configuration.TicketServiceEndPoint!)\(self.ticketEndPoint)?page=\(page!)&itemsPerPage=\(itemsPerPage!)"
        
        let url = URL(string: urlString)!
        
        let headers = HttpHeaderBuilder().addJsonContentType().addAuthenticationToken().Result()
        super.get(url: url, headers: headers, completion: callBack)
    }
}
