//
//  TicketService.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 13.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation


class BookingTicketService {
    private let ticketRepository = BookingTicketRepository()
    
    //delegates
    public var bookingTicketOperationDelegate: BookingTicketDelegate?
    public var bookedTicketsOperationDelegate: BookedTicketsOperationDelegate?
    public var bookedTicketOperationDelegate: BookedTicketOperationDelegate?
    
    public func bookTicket(ticket: BookTicketEntity) {
        ticketRepository.bookTicket(ticketToBook: ticket, callBack: bookingTicketOperationDidComplete)
    }
    
    public func bookedTicket(id: Int!) {
        ticketRepository.bookedTicket(id: id, callBack: bookedTicketOperationDidComplete)
    }
    
    public func bookedTickets(page: Int!, from: String?, to: String? ) {
        ticketRepository.bookedTickets(page: page, from: from, to: to, callBack: bookedTicketsOperationDidComplete)
    }
    
    //delegates
    private func bookedTicketOperationDidComplete(ticket: BookedTicket?, error: Error?){
         if let er = error {
            self.bookedTicketOperationDelegate?.bookedTicketOperationDidFail(error: er.localizedDescription)
         } else if let data = ticket {
             self.bookedTicketOperationDelegate?.bookedTicketOperationDidSuccess(ticket: data)
         }
     }
    
    private func bookedTicketsOperationDidComplete(tickets: Array<TicketPreview>?, error: Error?){
        if let er = error {
           self.bookedTicketsOperationDelegate?.bookedTicketsOperationDidFail(error: er.localizedDescription)
        } else if let data = tickets {
            self.bookedTicketsOperationDelegate?.bookedTicketsOperationDidSuccess(tickets: data)
        }
    }
    
    private func bookingTicketOperationDidComplete(ticket: Ticket?, error: Error?){
        if let er = error {
            self.bookingTicketOperationDelegate?.bookingTicketOperationDidFail(error: er.localizedDescription)
        } else if let data = ticket {
            self.bookingTicketOperationDelegate?.bookingTicketOperationDidSuccess(ticket: data)
        }
    }
}
