//
//  TicketCreationView.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 06.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct TicketCreationView: View {

    @ObservedObject private var ticketCreationViewModel: TicketCreationViewModel = TicketCreationViewModel()
    
    var body: some View {
        Form {
            VStack {
                VStack(alignment: .leading) {
                    HStack {
                        Image(systemName: "pencil")
                        Text("Tittle")
                            .font(.caption)
                            .autocapitalization(.none)
                    }
                    TextField("", text: $ticketCreationViewModel.ticketName)
                        .padding(.all, 5.0)
                        .overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.gray, lineWidth: 1)
                        )
                }
                
                Spacer(minLength: 20)
                
                VStack(alignment: .leading, spacing: 10, content: {
                    HStack {
                        Image(systemName: "pencil")
                        Text("Description")
                              .font(.caption)
                    }
                    
                    MultilineTextView(text: self.$ticketCreationViewModel.ticketDescription)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 100, maxHeight: 200, alignment: .topLeading)
                        .font(.caption)
                        .padding(5)
                        .overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.gray, lineWidth: 1)
                        )
                })
                
                HStack {
                    Image(systemName: "pencil")
                    Text("Amount of tickets:")
                        .font(.caption)
                    TextField("", text: $ticketCreationViewModel.ticketAmount)
                        .keyboardType(.numberPad)
                }.autocapitalization(.sentences)

                HStack {
                    Image(systemName: "pencil")
                    Text("Price of the ticket:")
                        .font(.caption)
                        .multilineTextAlignment(.leading)
                    TextField("Enter Price of the ticket", text: $ticketCreationViewModel.ticketPrice)
                        .keyboardType(.decimalPad)
                }

                HStack {
                    Image(systemName: "calendar")
                    DatePicker("Begin", selection: $ticketCreationViewModel.ticketDateStart)
                        .font(.caption)
                }
                
                Spacer(minLength: 20)
                
                HStack {
                    Image(systemName: "calendar")
                    DatePicker("End", selection: $ticketCreationViewModel.ticketDateEnd)
                        .font(.caption)
                }
    
                    
                Spacer(minLength: 20)
                HStack {
                    Spacer()
                    
                    Button(action: {
                        self.ticketCreationViewModel.creteTicket()
                    }) {
                        Text("Save")
                        .font(.headline)
                    }
                    .padding(.horizontal, 10.0)
                    .disabled(!self.ticketCreationViewModel.isValid)
                    
                    Spacer()
                }
            }
        }
    }
    
}

struct TicketCreationView_Previews: PreviewProvider {
    static var previews: some View {
        TicketCreationView()
    }
}
