//
//  TicketCreationOperationDelegate.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 11.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

protocol TicketCreationOperationDelegate {
    
    func eventTicketCreationOperationDidFail(error: String)
    
    func eventTicketCreationOperationDidSuccess(ticket: EventTicket)
}
