//
//  TicketCreationService.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class TicketCreationService {
    
    private let ticketRepository = TicketRepository()
    public var ticketCreationDelegate: TicketCreationOperationDelegate?
    
    public func createTicket(eventTicket: EventTicket) {
        ticketRepository.createTicket(eventTicket: eventTicket, callBack: eventCreationDidComplete)
    }
    
    private func eventCreationDidComplete(eventTicket: EventTicket?, error: Error?){
        if let er = error {
            self.ticketCreationDelegate?.eventTicketCreationOperationDidFail(error: er.localizedDescription)
        } else if let response = eventTicket {
            self.ticketCreationDelegate?.eventTicketCreationOperationDidSuccess(ticket: response)
        }
    }
}
