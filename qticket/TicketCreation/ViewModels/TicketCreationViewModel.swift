//
//  TicketCreationViewModel.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Combine
import Foundation

class TicketCreationViewModel: ObservableObject, TicketCreationOperationDelegate {

    private var ticketCreationService: TicketCreationService
    
    //input
    @Published var ticketName: String = ""
    
    @Published var ticketDescription: String = ""
        
    @Published var ticketAmount: String = "0"
    
    @Published var ticketPrice: String = "0"
    
    @Published var ticketDateStart = Date()
    
    @Published var ticketDateEnd = Date()
    
    //output
    @Published var isValid = false
    
    private var cs = Set<AnyCancellable>()
    init() {
        self.ticketCreationService = TicketCreationService()
        self.ticketCreationService.ticketCreationDelegate = self
        
        $ticketName
            .debounce(for:  0.1, scheduler: RunLoop.main)
            .map { input in
                return input.count < 100 && input.count > 0
        }
        .assign(to: \.isValid, on: self)
        .store(in: &cs)
    }
    
    func creteTicket() {
        if(isValid) {
            let model = EventAdapter.toEventTicket(ticketView: self)
            ticketCreationService.createTicket(eventTicket: model)
        }
    }
    
    private func reset() {
        DispatchQueue.main.async {
            self.ticketName = ""
            self.ticketDescription = ""
            self.ticketPrice = "0"
            self.ticketAmount = "0"
            self.ticketDateStart = Date()
            self.ticketDateEnd = Date()
        }
    }
    
    
    func eventTicketCreationOperationDidFail(error: String) {
        print(error)
    }
    
    func eventTicketCreationOperationDidSuccess(ticket: EventTicket) {
        print(ticket)
        reset()
    }
}
