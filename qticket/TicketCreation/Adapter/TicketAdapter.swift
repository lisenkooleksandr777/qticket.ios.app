//
//  TicketAdapter.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class EventAdapter {
    
   static func toEventTicket(ticketView: TicketCreationViewModel) -> EventTicket {
    let result = EventTicket()
        
    result.name = ticketView.ticketName;
    result.description = ticketView.ticketDescription
    result.price = Double(ticketView.ticketPrice)
    result.amount = Int(ticketView.ticketAmount)
    
    let isoFormatter = ISO8601DateFormatter()
    
    result.dateStart = isoFormatter.string(from: ticketView.ticketDateStart)
    result.dateEnd = isoFormatter.string(from: ticketView.ticketDateEnd)
    
    return result
    }
}
