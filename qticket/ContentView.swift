//
//  ContentView.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var selection = 2
 
    var body: some View {
        TabView(selection: $selection) {
            TicketScannerView()
                .tabItem {
                    HStack {
                        Image(systemName: "qrcode.viewfinder")
                        .imageScale(.large)
                    }
                }
                .tag(0)
            TicketCreationView()
                .tabItem {
                    HStack {
                        Image(systemName: "plus")
                        .imageScale(.large)
                    }
                }
                .tag(1)
            ProfileView()
                .tabItem {
                    HStack {
                        Image(systemName: "person")
                        .imageScale(.large)
                    }
                }
                .tag(2)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
