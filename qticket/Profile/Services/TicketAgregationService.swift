//
//  TicketAgregationService.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class TicketAgregationService {
    
    private let ticketRepository = TicketRepository()
    public var ticketListOperationDelegate: TicketListOperationDelegate?
    
    public func getOwnTickets(page: Int!, itemsPerPage: Int!) {
        ticketRepository.getOwnTickets(page: page, itemsPerPage: itemsPerPage, callBack: ticketListOperationDidComplete)
    }
    
    private func ticketListOperationDidComplete(tickets: Array<EventTicketPreview>?, error: Error?){
        if let er = error {
            self.ticketListOperationDelegate?.ticketListOperationDidFail(error: er.localizedDescription)
        } else if let data = tickets {
            self.ticketListOperationDelegate?.ticketListOperationDidSuccess(tickets: data)
        }
    }
}
