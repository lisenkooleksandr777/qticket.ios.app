//
//  BookedTicketsListViewModel.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 14.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class BookedTicketsViewModel: ObservableObject, BookedTicketsOperationDelegate {
    
    private var ticketService: BookingTicketService
    
    @Published var tickets: Array<TicketPreview> = Array<TicketPreview>()
    
    //ISO date for tickets section filter
    var from: String?
    var to: String?
    
    private var page = 0
    
    init() {
        self.ticketService = BookingTicketService()
        self.ticketService.bookedTicketsOperationDelegate = self
    }
    
    init(from: String?, to: String?) {
        self.ticketService = BookingTicketService()
        self.ticketService.bookedTicketsOperationDelegate = self
        self.from = from
        self.to = to
     }
    
    func bookedTicketsOperationDidFail(error: String) {
        print(error)
    }
    
    func bookedTicketsOperationDidSuccess(tickets: Array<TicketPreview>) {
        DispatchQueue.main.async() {
            if(self.page == 0) {
                self.reInit(data: tickets)
            } else {
                if(tickets.count > 0){
                    self.merge(data: tickets)
                }
            }
            self.page += 1;
        }
    }
    
    func initialLoad() {
       self.ticketService.bookedTickets(page: page, from: from, to: to)
    }
    
    func nextPage() {
        //self.ticketService.booked
    }
    
    private func reInit(data: Array<TicketPreview>) {
        self.tickets = Array<TicketPreview>();
        
        for ticket in data {
            self.tickets.append(ticket)
        }
    }
    
    private func merge(data: Array<TicketPreview>) {
        for ticket in data {
            self.tickets.append(ticket)
        }
    }
}
