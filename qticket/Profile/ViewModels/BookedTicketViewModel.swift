//
//  BookedTicketsListViewModel.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 14.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class BookedTicketViewModel: ObservableObject, BookedTicketOperationDelegate {

    private var ticketService: BookingTicketService
    
    @Published var ticket: BookedTicket?
    @Published var isLoading = true;
    
    
    init() {
        self.ticketService = BookingTicketService()
        self.ticketService.bookedTicketOperationDelegate = self
     }
    
    func load(id: Int) {
        self.isLoading = true;
        self.ticketService.bookedTicket(id: id)
    }
    
    func getQRURL(id: Int) -> String {
        return Configuration.getBookedTicketQRURL(id: id)
    }
    
    func bookedTicketOperationDidFail(error: String) {
        print(error)
        self.isLoading = false;
    }
    
    func bookedTicketOperationDidSuccess(ticket: BookedTicket) {
        DispatchQueue.main.async() {
            self.ticket = ticket
            self.isLoading = false;
        }
    }
    
    func getDateRange() -> String {
        return DateTimeUtil.GetISO8601DateRange(from: ticket!.event.dateStart, to: ticket!.event.dateEnd)
      }
    
}
