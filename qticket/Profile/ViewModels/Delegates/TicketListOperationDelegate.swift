//
//  TicketListViewOperationDelegate.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

protocol TicketListOperationDelegate {
    
    func ticketListOperationDidFail(error: String)
    
    func ticketListOperationDidSuccess(tickets: Array<EventTicketPreview>)
}
