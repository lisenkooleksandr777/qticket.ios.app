//
//  OwnTicketsListViewModel.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI
import Combine

class OwnTicketsListViewModel: ObservableObject, TicketListOperationDelegate {

    @Published var tickets: Array<EventTicketPreview> = Array<EventTicketPreview>()
    
    private var ticketAgregationService = TicketAgregationService()

    private var page = 0
    private var itemsPerPage = 10;
    
    
    func ticketListOperationDidFail(error: String) {
        print(error)
    }
    
    func ticketListOperationDidSuccess(tickets: Array<EventTicketPreview>) {
        DispatchQueue.main.async() {
            if(self.page == 0) {
                self.reInit(data: tickets)
            } else {
                if(tickets.count > 0){
                    self.merge(data: tickets)
                }
            }
            self.page += 1;
        }
    }
    
    func initialLoad() {
        self.ticketAgregationService.ticketListOperationDelegate = self
        self.ticketAgregationService.getOwnTickets(page: self.page, itemsPerPage: self.itemsPerPage)
    }
    
    func nextPage() {
        self.ticketAgregationService.getOwnTickets(page: self.page, itemsPerPage: self.itemsPerPage)
    }
    
    private func reInit(data: Array<EventTicketPreview>) {
        self.tickets = Array<EventTicketPreview>();
        
        for ticket in data {
            self.tickets.append(ticket)
        }
    }
    
    private func merge(data: Array<EventTicketPreview>) {
        for ticket in data {
            self.tickets.append(ticket)
        }
    }
}
