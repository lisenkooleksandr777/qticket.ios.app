//
//  ProfileView.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/29/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
    @Environment(\.imageCache) var cache: ImageCache
    
    var body: some View {
        NavigationView {
            VStack(spacing: 10) {
                Spacer(minLength: 10)
                AsyncImage(
                        url:  URL(string: SessionDataHolder.Claims.pictureURL!)!,
                        cache: self.cache,
                        placeholder: Text("Loading ..."),
                        useAuth: false,
                        configuration: { $0.resizable() }
                )
                .frame(width: 100.0, height: 100.0)
                    .cornerRadius(50)
                    .overlay(RoundedRectangle(cornerRadius: 50)
                    .stroke(Color.green, lineWidth: 1))
                    .shadow(radius: 10)
                
                Text(SessionDataHolder.Claims.name)
                    .font(.subheadline)
                  
                List {
                    Section(header:
                        HStack {
                            Image(systemName: "qrcode")
                            Text("Tickets")
                        }) {
                            NavigationLink(destination: BookedTicketsView(viewModel: BookedTicketsViewModel(from: DateTimeUtil.formatDateISO8601(date: Date().startOfDay), to: DateTimeUtil.formatDateISO8601(date: Date().endOfDay)))) {
                            Text("Today")
                        }
                        
                            NavigationLink(destination: BookedTicketsView(viewModel: BookedTicketsViewModel(from: DateTimeUtil.formatDateISO8601(date: Date().endOfDay), to: nil))) {
                            Text("Future")
                        }
                            
                        NavigationLink(destination: BookedTicketsView(viewModel: BookedTicketsViewModel())) {
                            Text("All")
                        }
                        
                        NavigationLink(destination: BookedTicketsView(viewModel: BookedTicketsViewModel(from: nil, to: DateTimeUtil.formatDateISO8601(date: Date().startOfDay)))) {
                            Text("Archive")
                        }
                    }
                    
                    Section(header:
                        HStack {
                            Image(systemName: "wrench")
                            Text("Manage Tickets")
                        }){
                        NavigationLink(destination: OwnTicketsListView()) {
                            Text("Own Tickets")
                        }
                    }

                    Section(header:
                        HStack {
                            Image(systemName: "app")
                            Text("App")
                        }) {
                        NavigationLink(destination: SettingsView()) {
                            Text("Settings")
                        }
                            
                        NavigationLink(destination: PolicyView()) {
                            Text("Policy")
                        }
                    }
                }
                .listStyle(GroupedListStyle())
                .environment(\.horizontalSizeClass, .none)
            }
            .navigationBarTitle("Profile", displayMode: .inline)
            .navigationBarHidden(true)
            .font(.subheadline)
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
