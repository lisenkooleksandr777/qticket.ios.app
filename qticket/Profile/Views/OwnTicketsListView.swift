//
//  OwnTicketsListView.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct OwnTicketsListView: View {
    
    @Environment(\.imageCache) var cache: ImageCache
    @ObservedObject var viewModel: OwnTicketsListViewModel = OwnTicketsListViewModel()
    
    var body: some View {
        VStack{
            List {
                ForEach(viewModel.tickets, id: \.id) { item in
                    NavigationLink(destination: OwnEventView()) {
                        HStack {
                            Image(systemName: "qrcode")
                            Text(item.name)
                        }
                    }
                }
            }
        }
        .onAppear {
            self.viewModel.initialLoad()
        }
    }
}

struct OwnTicketsListView_Previews: PreviewProvider {
    static var previews: some View {
        OwnTicketsListView()
    }
}
