//
//  BookedTicketsView.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 14.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct BookedTicketsView: View {
    
    @ObservedObject var viewModel: BookedTicketsViewModel
    
    var body: some View {
        VStack{
            List {
                ForEach(viewModel.tickets, id: \.id) { item in
                    NavigationLink(destination: BookedTicketView(ticketId: item.id)) {
                        HStack {
                            Image(systemName: "qrcode")
                            Text(item.name)
                        }
                    }
                }
            }
        }
        .onAppear {
            self.viewModel.initialLoad()
        }
    }
    
}
