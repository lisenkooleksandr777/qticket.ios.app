//
//  BookedTicketView.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 14.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct BookedTicketView: View {
     @Environment(\.imageCache) var cache: ImageCache
    
    @ObservedObject private var bookedTicketViewModel = BookedTicketViewModel()
    public var ticketId: Int
    
    @ViewBuilder
    var body: some View {
        VStack {
            if bookedTicketViewModel.isLoading  {
                Text("Loading...")
            } else {
                HStack {
                    AsyncImage(
                        url:  URL(string: bookedTicketViewModel.getQRURL(id: ticketId))!,
                            cache: self.cache,
                            placeholder: Text("Loading ..."),
                            useAuth: true,
                            configuration: { $0.resizable() }
                    )
                    .aspectRatio(1, contentMode: .fit)
                    .shadow(radius: 10)
                }
                                    
                HStack {
                    Image(systemName: "qrcode")
                    Text(bookedTicketViewModel.ticket!.event.name)
                        .font(.headline)
                        
                    Spacer()
                            
                    Image(systemName: "calendar")
                                                   
                    Text(bookedTicketViewModel.getDateRange())
                        .font(.caption)
                }
                    
                HStack {
                    Image(systemName: "person")
                        
                    Text("Visitor:")
                        .font(.subheadline)
                        
                    Text(bookedTicketViewModel.ticket!.visitorName)
                        .font(.headline)
                        
                    Spacer()
                }
                Spacer()
            }
        }
        .padding(10)
        .onAppear {
            self.bookedTicketViewModel.load(id: self.ticketId)
        }
    }
    
}
