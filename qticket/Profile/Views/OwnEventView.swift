//
//  OwnEventView.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 08.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct OwnEventView: View {
    var body: some View {
        Text("Own event")
    }
}

struct OwnEventView_Previews: PreviewProvider {
    static var previews: some View {
        OwnEventView()
    }
}
