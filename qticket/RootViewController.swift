//
//  RootViewController.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/28/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class RootViewController: UIViewController, RootViewControllerDelegate {
    
    
    init() {
        self.current = SplashViewController()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private var current: UIViewController!
    override func viewDidLoad(){
        super.viewDidLoad()
    
        self.view.backgroundColor = UIColor.white
        
        addChild(current)
        current?.view.frame = view.bounds
        view.addSubview(current.view)
        current.didMove(toParent: self)
        
        let authController = AuthenticationController()
        authController.rootDelegate = self as RootViewControllerDelegate
        animateFadeTransition(to: authController)
    }
        
    public func toContentViewController() {
        let contentConroller = UIHostingController(rootView: ContentView())
        animateFadeTransition(to: contentConroller)
    }
    
    private func animateFadeTransition(to new: UIViewController, completion: (() -> Void)? = nil) {
        self.current.willMove(toParent: nil)
        addChild(new)
           
        transition(from: current, to: new, duration: 0.3, options: [.transitionCrossDissolve, .curveEaseOut], animations: {
        }) { completed in
            self.current.removeFromParent()
            
            new.view.frame = self.view.bounds
            new.didMove(toParent: self)
            self.view.addSubview(new.view)
            
            self.current = new
            completion?()
        }
    }
}

/*
 RootViewController delegate wich provides capability to redirect for content view once authentication success
*/
protocol RootViewControllerDelegate {
    func toContentViewController()
}
