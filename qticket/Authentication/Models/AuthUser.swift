//
//  AuthUser.swift
//  e
//
//  Created by Oleksandr Lisenko on 24.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

public class AuthUser: Decodable,Encodable {
        
    public var name: String?;
    public var gender: String?;
    public var birthday: String?;
    public var email: String?;
    public var pictureURL: String?;

    public var faceBookToken: FacebookToken?;
    public var googleToken: GoogleToken?;
    
    public init() {
    }
}
