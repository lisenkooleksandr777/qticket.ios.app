//
//  GoogleToken.swift
//  e
//
//  Created by Oleksandr Lisenko on 24.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

public class GoogleToken: Decodable, Encodable {
    public var userId: String?
    public var tokenString: String?
    public var tokenId: String?
    public var provider = "GOOGLE"
}
