//
//  AuthenticationController.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import SwiftUI
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class AuthenticationController: UIViewController, LoginButtonDelegate, AuthenticationDelegate, GIDSignInDelegate {

    //* private variables *//
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    
    private let authService = AuthenticationService()
    private let loginManager = LoginManager()
    
    public var rootDelegate : RootViewControllerDelegate!
    //********************************************//
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        authService.authenticationDelagate = self
        
        // Initialize Google sign-in
        GIDSignIn.sharedInstance().clientID = "518662720847-6sjps5hpm885j17lnpb0mqdfevqq42gc.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        
        self.view.backgroundColor = UIColor.white
        self.tryAuth()
    }
    
    //* Implementation of AuthenticationDelegate *//
    func authenticationDidCompleteSuccess(_ authResult: AuthResult) {
         DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)){
            if let t = authResult.token {
                SessionDataHolder.JWTToken = t
                SessionDataHolder.Claims = authResult.userData
                self.stopLoading()
                self.rootDelegate.toContentViewController()
            } else {
                self.authenticationDidCompleteFail("Error during authentication")
            }
        }
    }
    
    func authenticationDidCompleteFail(_ error: String) {
        DispatchQueue.main.async() {
            self.stopLoading()
            let alrt = ErrorAlertFactory.GetErrorAlert(message: error)
            
            let okAction = UIAlertAction(title: LocalizationUtil.GetLocalizationString(id: "OK") , style: UIAlertAction.Style.cancel) { (action) in
                //self.addFaceBookLoginButton()
                self.addGoogleLogininButton()
            }
            
            let retryAction = UIAlertAction(title: LocalizationUtil.GetLocalizationString(id: "Retry") , style: UIAlertAction.Style.default) { (action) in
                self.tryAuth()
            }
            
            alrt.addAction(okAction)
            alrt.addAction(retryAction)
    
            self.present(alrt, animated: true, completion: nil)
        }
    }
    
    //********************************************//
    
    //* Authentication *//
    private func showLoading() {
        view.backgroundColor = UIColor.white
        self.activityIndicator.color = UIColor.gray
        view.addSubview(activityIndicator)
        activityIndicator.frame = view.bounds
        activityIndicator.startAnimating()
    }
    
    private func stopLoading() {
        self.activityIndicator.stopAnimating()
    }
    
    private func tryAuth() {
        //TODO: add cacheing of JWT token
        if AccessToken.isCurrentAccessTokenActive {
            self.getFacebooklUserData()
        } else {
            //self.addFaceBookLoginButton()
            self.addGoogleLogininButton()
        }
    }
    
    private func authenticateByToken(JWTtoken: String){
        self.authService.authByToken(JWTtoken: JWTtoken)
    }
    
    private func addGoogleLogininButton() {
        GIDSignIn.sharedInstance()?.delegate = self
        let loginButton = GIDSignInButton(frame: CGRect(origin: CGPoint(x: view.frame.width * 0.1, y: view.frame.height * 0.5 - 60), size: CGSize(width: view.frame.width * 0.8, height: 50)))
        self.view.addSubview(loginButton)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
          if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
            print("The user has not signed in before or they have since signed out.")
          } else {
            print("\(error.localizedDescription)")
          }
          // [START_EXCLUDE silent]
          NotificationCenter.default.post(
            name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
          // [END_EXCLUDE]
          return
        }
        self.stopLoading()
        self.authService.authByGoogleUser(googleUserData: user)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
      NotificationCenter.default.post(
        name: Notification.Name(rawValue: "ToggleAuthUINotification"),
        object: nil,
        userInfo: ["statusText": "User has disconnected."])
    }
    
    private func addFaceBookLoginButton(){
        let loginButton = FBLoginButton(frame: CGRect(origin: CGPoint(x: view.frame.width * 0.1, y: view.frame.height * 0.5 + 10), size: CGSize(width: view.frame.width * 0.8, height: 40)))
        loginButton.permissions = ["public_profile", "email", "user_birthday", "user_gender", "user_events"]
        loginButton.delegate = self
        self.view.addSubview(loginButton)
    }
        
    private func getFacebooklUserData(){
        self.showLoading()
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(normal), email, birthday, gender"])
            .start(completionHandler: { (connection, result, error) -> Void in
                if let err = error {
                    self.authenticationDidCompleteFail(err.localizedDescription)
                } else {
                    guard let userFaceBookData = (result as? [String : AnyObject]) else {
                        self.addFaceBookLoginButton()
                        return
                    }
                    self.authService.authByFaceBookUser(faceBookUserData: userFaceBookData, idToken: AccessToken.current?.tokenString)
                }
            }
        )
    }
    
    //* Implementation of LoginButtonDelegate *//
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if let err = error {
            self.authenticationDidCompleteFail(err.localizedDescription)
        } else if let _ = AccessToken.current {
            self.getFacebooklUserData()
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
    }
    
    //********************************************//
}
