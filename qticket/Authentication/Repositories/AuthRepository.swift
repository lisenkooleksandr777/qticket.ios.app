//
//  AuthRepository.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation


class AuthRepository: BaseRepository {
    
    private let authByEntityEndPoint = "/api/v1/Auth"
    
    //function will create new user in case user does not exist in the system or will authenticate existing one
    func auth(authUser: AuthUser, callBack: @escaping (AuthResult?, Error?) -> Void) {
        let url = URL(string: "\(Configuration.AuthServiceEndPoint!)\(self.authByEntityEndPoint)")!
        
        let headers = HttpHeaderBuilder().addJsonContentType().Result()
        super.post(data: authUser, url: url, headers: headers, completion: callBack)
    }
    
    /*
    func authByToken(JWTtoken: String?, callBack: @escaping (AuthResult?, Error?) -> Void) {
        let url = URL(string: "\(Configuration.AuthServiceEndPoint!)\(self.authByTokenEndPoint)")!
        
        var headers = HttpHeaderBuilder().addJsonContentType().Result()
        headers["Authorization"] = "Bearer \(String(describing: JWTtoken))"
        super.get(url: url, headers: headers, completion: callBack)
    }
 */
}
