//
//  TicketScannerService.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 10.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class TickeScannerService {
    
    private let scannerRepository = TicketScanerRepository()
    public var ticketScanningOperationDelegate: TicketScanningOperationDelegate?
    
    public func scann(qr: String) {
        scannerRepository.getTicketByQR(qr: qr, callBack: ticketScanningOperationDidComplete)
    }
    
    private func ticketScanningOperationDidComplete(ticket: QRScannerResult?, error: Error?){
        if let er = error {
            self.ticketScanningOperationDelegate?.ticketScanningOperationDidFail(error: er.localizedDescription)
        } else if let data = ticket {
            self.ticketScanningOperationDelegate?.ticketScanningOperationDidSuccess(ticket: data)
        }
    }
}
