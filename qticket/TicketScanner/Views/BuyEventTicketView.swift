//
//  BuyEventTicketView.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 09.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct BuyEventTicketView: View {
    
    @Environment(\.imageCache) var cache: ImageCache
    
    @ObservedObject var viewModel: BuyTicketViewModel
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                HStack {
                    AsyncImage(
                        url:  URL(string: viewModel.getQRURL())!,
                            cache: self.cache,
                            placeholder: Text("Loading ..."),
                            useAuth: false,
                            configuration: { $0.resizable() }
                    )
                    .aspectRatio(1, contentMode: .fit)
                    .shadow(radius: 10)
                }
                
                HStack(){
                    Text(viewModel.eventTicketAnalytic.eventTicket.name)
                    .font(.headline)
                    Spacer()
                }
                .padding(10)
                .background(Color.white)
     
                
                HStack{
                    Text(viewModel.eventTicketAnalytic.eventTicket.description)
                        .font(.caption)
                    Spacer()
                }
                .padding(10)
                
                
                HStack{
                    Text(viewModel.getAvailableTicketsCount())
                    .font(.caption)
                    
                    Spacer()
                    
                    Image(systemName: "calendar")
                    Text(viewModel.getDateRange())
                        .font(.caption)
                }
                .padding(10)
                
                
                Spacer(minLength: 40)
                
                if(!self.viewModel.showBuyTicketForm) {
                    
                    HStack {
                        Spacer()
                        
                        Button(action: {
                            self.viewModel.showTicketForm()
                        }) {
                            Text("Order ticket")
                                .font(.caption)
                        }

                        Spacer()
                    }
                } else {
                    VStack {
                     
                        TextField("Enter visitor name", text: $viewModel.visitorName)
                        .font(.subheadline)
                        
                        HStack {
                            Button(action: {
                                self.viewModel.cancel()
                            }) {
                                Text("Cancel")
                                .font(.caption)
                            }
                            
                            Button(action: {
                                self.viewModel.buyTicket()
                            }) {
                                Text("Buy")
                                .font(.caption)
                            }
                        }
                    }
                }
                Spacer()
            }
            .padding(10)
        }
    }
}

