//
//  CheckTicketView.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 09.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct CheckTicketView: View {
    @Environment(\.imageCache) var cache: ImageCache

    var viewModel: CheckTicketViewModel

    var body: some View {
        VStack(alignment: .leading) {
            if viewModel.ticket.owner.pictureURL != nil {
                HStack {
                    Spacer()
                    AsyncImage(
                        url:  URL(string: viewModel.ticket.owner.pictureURL!)!,
                            cache: self.cache,
                            placeholder: Text("Loading ..."),
                            useAuth: false,
                            configuration: { $0.resizable() }
                    )
                    .frame(width: 100.0, height: 100.0)
                        .cornerRadius(50)
                        .overlay(RoundedRectangle(cornerRadius: 50)
                        .stroke(Color.green, lineWidth: 1))
                        .shadow(radius: 10)
                    Spacer()
                }
            }
            HStack {
                Spacer()
                Text(viewModel.ticket.owner.name)
                    .font(.subheadline)
                Spacer()
            }
            
            HStack {
                Image(systemName: "qrcode")
                Text(viewModel.ticket.event.name)
                    .font(.headline)
                
                Spacer()
                    
                Image(systemName: "calendar")
                                           
                Text(viewModel.getDateRange())
                    .font(.caption)
            }
            
            
            HStack {
                Image(systemName: "person")
                
                Text("Visitor:")
                    .font(.subheadline)
                
                Text(viewModel.ticket.visitorName)
                .font(.headline)
                
                Spacer()
            }
            
            Spacer()
        }
        .padding(10)
    }
}

