//
//  TicketScannerView.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 06.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI
import CodeScanner

struct TicketScannerView: View {
    
    @ObservedObject private var ticketScannerViewModel =  TicketScannerViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                if ticketScannerViewModel.isShowingScanner {
                    CodeScannerView(codeTypes: [.qr], simulatedData: "", completion: self.handleScan)
                }
                else if ticketScannerViewModel.eventTicketAnalytic != nil {
                     BuyEventTicketView(viewModel: BuyTicketViewModel(eventTicketAnalytic: ticketScannerViewModel.eventTicketAnalytic!))
                }
                else if self.ticketScannerViewModel.ticket != nil {
                    CheckTicketView(viewModel: CheckTicketViewModel(ticket:self.ticketScannerViewModel.ticket!))
                } else {
                    Text("Loading")
                }
            }
            .navigationBarTitle("QR Scanner", displayMode: .inline)
            .navigationBarItems(leading:
                HStack {
                    if !ticketScannerViewModel.isShowingScanner {
                        Button(action: {
                            self.ticketScannerViewModel.reset()
                        }) {
                            Image(systemName: "arrow.left")
                                .imageScale(.large)
                        }
                    }
                }
            )
        }
    }
    
    private func handleScan(result: Result<String, CodeScannerView.ScanError>) {
       switch result {
       case .success(let data):
            self.ticketScannerViewModel.scann(qr: data)
       case .failure(let error):
           print("Scanning failed \(error)")
       }
    }
    
}

struct TicketScannerView_Previews: PreviewProvider {
    static var previews: some View {
        TicketScannerView()
    }
}
