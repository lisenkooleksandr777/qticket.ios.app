//
//  ButTicketViewModel.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 10.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class BuyTicketViewModel: ObservableObject, BookingTicketDelegate {

    private var bookingTicketService: BookingTicketService
    
    var eventTicketAnalytic: EventTicketAnalytic!
    
    @Published var showBuyTicketForm = false
    @Published var visitorName: String = ""
    
    
    init(eventTicketAnalytic: EventTicketAnalytic) {
        self.eventTicketAnalytic = eventTicketAnalytic;
        
        self.bookingTicketService = BookingTicketService()
        self.bookingTicketService.bookingTicketOperationDelegate = self
    }
    
    func bookingTicketOperationDidFail(error: String) {
        print(error)
    }
    
    func bookingTicketOperationDidSuccess(ticket: Ticket) {
        print(ticket)
        self.resetForm()
    }
    
    func buyTicket() {
        if self.validate() {
            let bookingEntity = BookTicketEntity()
            bookingEntity.visitorName = self.visitorName
            bookingEntity.event = eventTicketAnalytic.eventTicket
            self.bookingTicketService.bookTicket(ticket: bookingEntity)
        }
    }
    
    func getAvailableTicketsCount() -> String {
        if(eventTicketAnalytic.amountOfAvailableTickets == 0) {
            return "All tickets are sold"
        }
        return "\(eventTicketAnalytic.amountOfAvailableTickets!) of \(eventTicketAnalytic.eventTicket.amount!) tickets available"
    }
    
    func getDateRange() -> String {
        return DateTimeUtil.GetISO8601DateRange(from: eventTicketAnalytic.eventTicket.dateStart, to: eventTicketAnalytic.eventTicket.dateEnd)
    }
    
    func getQRURL() -> String {
        let url = "\(Configuration.TicketServiceEndPoint!)/api/v1/TicketAgregator/qr/\(eventTicketAnalytic.eventTicket.id!)"
        return url
    }
    
    func showTicketForm() {
        self.showBuyTicketForm = true;
    }
    
    func resetForm() {
        self.visitorName = ""
    }
    
    func cancel() {
        self.showBuyTicketForm = false;
        self.resetForm()
    }
    
    func validate() -> Bool {
        return true
    }
}
