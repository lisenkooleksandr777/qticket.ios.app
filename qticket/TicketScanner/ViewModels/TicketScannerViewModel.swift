//
//  TocketScannerViewModel.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 10.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class TicketScannerViewModel: ObservableObject, TicketScanningOperationDelegate {
    
    @Published var eventTicketAnalytic: EventTicketAnalytic?
    @Published var ticket: Ticket?
    @Published var isShowingScanner = true;
    
    private var ticketScannerService = TickeScannerService()
    
    func scann(qr: String) {
        self.ticketScannerService.ticketScanningOperationDelegate = self
        self.ticketScannerService.scann(qr: qr)
    }

    func reset() {
        self.isShowingScanner = true
        self.eventTicketAnalytic = nil
        self.ticket = nil
    }
    
    func ticketScanningOperationDidFail(error: String) {
        print(error)
    }
    
    func ticketScanningOperationDidSuccess(ticket: QRScannerResult) {
        DispatchQueue.main.async() {
            self.isShowingScanner = false
            if let ticketAnalytic = ticket.eventTicketAnalytic {
                self.eventTicketAnalytic = ticketAnalytic
            } else if let ticket = ticket.ticket {
                self.ticket = ticket
            }
        }
    }
}
