//
//  CheckTicketViewModel.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 10.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class CheckTicketViewModel: ObservableObject {
    
    var ticket: Ticket!
    
    init(ticket: Ticket) {
        self.ticket = ticket;
    }
    
    
    func getDateRange() -> String {
         return DateTimeUtil.GetISO8601DateRange(from: ticket.event.dateStart, to: ticket.event.dateEnd)
     }
}
