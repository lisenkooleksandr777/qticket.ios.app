//
//  TicketScannerRepository.swift
//  qticket
//
//  Created by Oleksandr Lisenko on 10.06.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class TicketScanerRepository: BaseRepository {
    private let ticketScannerEndPoint = "/api/v1/QR"
    
    func getTicketByQR(qr: String, callBack: @escaping (QRScannerResult?, Error?) -> Void) {
        let url = URL(string: "\(Configuration.TicketServiceEndPoint!)\(self.ticketScannerEndPoint)/\(qr)")!
        
        let headers = HttpHeaderBuilder().addJsonContentType().addAuthenticationToken().Result()
        super.get(url: url, headers: headers, completion: callBack)
    }
}
